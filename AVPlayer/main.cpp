// ffmpeg_Console.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <Windows.h>

#include "AVPlayer.h"

#define DLL_Library
#ifdef DLL_Library

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)

{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return TRUE;
}
#else
int main(int argc, char* argv[])
{
	auto lpszMediaPath = "E:\\Code\\other\\AVPlayer\\testMedia\\test.mp4";

	//mp3topcm(lpszMediaPath, "E:\\Code\\other\\ffmpeg_Console\\testVideo\\test.pcm");
	//return 0;
	auto pAVplayer = CreateAVPlayer();

	pAVplayer->initPlayer();
	pAVplayer->openMedia(lpszMediaPath);
	pAVplayer->play();
	pAVplayer->wait();
	delete pAVplayer;
	return 0;
}
#endif