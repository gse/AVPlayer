#include "stdafx.h"
#include "BufferCache.h"
#include <algorithm>

CBufferCache::CBufferCache()
{
}


CBufferCache::~CBufferCache()
{
	if (m_pBUf)
		delete[]m_pBUf;
}

bool CBufferCache::initBuf(const unsigned long & uBufSize, const unsigned char * pBuf)
{
	assert(m_pBUf == nullptr);
	assert(uBufSize > 0);
	m_pBUf = new unsigned char[uBufSize];
	if (pBuf)
		memcpy(m_pBUf, pBuf, uBufSize);
	else
		memset(m_pBUf, 0, uBufSize);
	m_uBufSize = uBufSize;
	m_pLastPos = m_pBUf + uBufSize;
	m_pWritePos = m_pBUf;
	m_pReadPos = m_pBUf;
	return true;
}

int CBufferCache::copyBuf(const unsigned long & uBufSize, unsigned char * pBuf)
{
	assert(uBufSize > 0);
	auto bufCanReadSize = m_pWritePos - m_pReadPos;
	assert(bufCanReadSize > 0);
	if (bufCanReadSize <= 0)
		return false;
	unsigned long uCopySize = std::min(uBufSize, (unsigned long)bufCanReadSize);
	memcpy(pBuf, m_pReadPos, uCopySize);
	m_pReadPos += uCopySize;
	return uCopySize;
}

int CBufferCache::appendBuf(const unsigned long & uBufSize, const unsigned char * pBuf)
{
	assert(uBufSize > 0 && pBuf != nullptr);
	long nRemainLen = m_pLastPos - m_pWritePos;
	assert(nRemainLen >= 0);
	if (nRemainLen < uBufSize)//此处要考虑初始设置的总长度小于要求的buffer长度
		return 0;

	memcpy(m_pWritePos, pBuf, uBufSize);
	m_pWritePos += uBufSize;
	return 0;
}

void CBufferCache::clearBuf()
{
	assert(m_pBUf != nullptr);
	m_pLastPos = m_pBUf;
	m_pWritePos = m_pBUf;
	m_pReadPos = m_pBUf;
	memset(m_pBUf, 0, m_uBufSize);
	m_pLastPos = m_pBUf + m_uBufSize;
}

const unsigned char *const  CBufferCache::readBuf(const unsigned long& uLen)
{
	assert(uLen >= 0);
	auto bufCanReadSize = m_pWritePos - m_pReadPos;
	assert(bufCanReadSize > 0);
	if (bufCanReadSize <= 0 || uLen > bufCanReadSize)
		return false;
	
	auto pReadPos = m_pReadPos;
	m_pReadPos += uLen;
	return pReadPos;
}
