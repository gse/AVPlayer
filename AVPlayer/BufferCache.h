#pragma once
class CBufferCache
{
public:
	CBufferCache();
	~CBufferCache();

	bool		initBuf(const unsigned long& uBufSize, const unsigned char* pBuf = nullptr);
	int			copyBuf(const unsigned long& uBufSize, unsigned char* pBuf);
	int			appendBuf(const unsigned long& uBufSize, const unsigned char* pBuf);
	void		clearBuf();
	unsigned long	getFreeSize()const { return m_pLastPos - m_pWritePos; }
	unsigned long	getRemainDataSize()const { return m_pWritePos - m_pReadPos; }
	const unsigned char* const	readBuf(const unsigned long& uLen);
private:
	unsigned long	m_uBufSize;
	unsigned char*	m_pBUf;
	unsigned char*	m_pReadPos;
	unsigned char*	m_pWritePos;
	unsigned char*	m_pLastPos;
};

