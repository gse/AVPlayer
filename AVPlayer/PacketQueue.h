#pragma once
struct AVPacket;
struct SDL_mutex;

class CPacketQueue//ѭ������
{
public:
	CPacketQueue(const int& nMaxSize = 100);
	~CPacketQueue();

	AVPacket*	front();
	AVPacket*	popFront();

	bool		push(AVPacket* pPacket);

	int			getMaxSize() { return m_nMaxSize; }
	int			getSize();

	bool		isFull();
	bool		isEmpty();	
private:
	int		m_nFrontIndex;
	int		m_nBackIndex;

	int		m_nMaxSize;
	int		m_nSize;

	AVPacket**	m_pPacketAry;
	SDL_mutex*	m_pAryMutex;
};

