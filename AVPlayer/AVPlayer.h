#pragma once
class IAVPlayer
{
public:
	virtual ~IAVPlayer(){}
	virtual bool		initPlayer(void* parentWindow = nullptr, const unsigned long& uInitFlag = 0) = 0;
	virtual bool		openMedia(const char* lpszMediaPath) = 0;
	virtual bool		play() = 0;
	virtual bool		stop() = 0;
	virtual bool		pause() = 0;
	virtual int			wait() = 0;
};

IAVPlayer*__stdcall CreateAVPlayer();