
// MFCPlayerDlg.h : header file
//

#pragma once

class IAVPlayer;

// CMFCPlayerDlg dialog
class CMFCPlayerDlg : public CDialogEx
{
// Construction
public:
	CMFCPlayerDlg(CWnd* pParent = NULL);	// standard constructor
	~CMFCPlayerDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCPLAYER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()


private:
	IAVPlayer* m_pAVPlayer;
public:
	afx_msg void OnBnClickedBtnPlay();
	afx_msg void OnBnClickedBtnStop();
};
